# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Api.Repo.insert!(%Api.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

%Api.API.Batsignal{}
|> Api.API.Batsignal.changeset(%{signal: true})
|> Api.Repo.insert!

%Api.API.User{}
|> Api.API.User.changeset(
     %{
       "username" => "admin",
       "email" => "admin@gmail.com",
       "password" => "password",
        "role" => "admin"
     }
   )
|> Api.API.User.changeset_role(%{role: "admin"})
|> Api.Repo.insert!