defmodule Api.Repo.Migrations.CreateClocking do
  use Ecto.Migration

  def change do
    create table(:clocking) do
      add :time, :naive_datetime
      add :status, :boolean, default: false, null: false
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:clocking, [:user_id])
  end
end
