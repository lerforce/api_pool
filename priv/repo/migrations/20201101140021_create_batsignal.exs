defmodule Api.Repo.Migrations.CreateBatsignal do
  use Ecto.Migration

  def change do
    create table(:batsignal) do
      add :signal, :boolean, default: false, null: false

      timestamps()
    end

  end
end
