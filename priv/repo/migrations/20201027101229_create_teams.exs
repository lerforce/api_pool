defmodule Api.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:teams, [:user_id])
  end
end
