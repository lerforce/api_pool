defmodule Api.Repo.Migrations.CreateUsersTeams do
  use Ecto.Migration

  def change do
    create table(:users_teams) do
      add :user_id, references(:users, on_delete: :nothing)
      add :teams_id, references(:teams, on_delete: :nothing)

      timestamps()
    end

    create index(:users_teams, [:user_id])
    create index(:users_teams, [:teams_id])
  end
end
