defmodule Api.Repo.Migrations.AddTeamsIdInUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :teams_id, references(:teams, on_delete: :nothing)
    end
    create index(:users, [:teams_id])
  end
end
