defmodule Api.Repo.Migrations.AddNameToTeam do
  use Ecto.Migration

  def change do
    alter table(:teams) do
      add :name, :string
    end
  end
end
