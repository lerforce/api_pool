defmodule Api.ClockingControllerTest do
  use Api.ConnCase

  alias Api.Clocking
  @valid_attrs %{status: true, time: ~N[2010-04-17 14:00:00.000000]}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, clocking_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing clocking"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, clocking_path(conn, :new)
    assert html_response(conn, 200) =~ "New clocking"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, clocking_path(conn, :create), clocking: @valid_attrs
    clocking = Repo.get_by!(Clocking, @valid_attrs)
    assert redirected_to(conn) == clocking_path(conn, :show, clocking.id)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, clocking_path(conn, :create), clocking: @invalid_attrs
    assert html_response(conn, 200) =~ "New clocking"
  end

  test "shows chosen resource", %{conn: conn} do
    clocking = Repo.insert! %Clocking{}
    conn = get conn, clocking_path(conn, :show, clocking)
    assert html_response(conn, 200) =~ "Show clocking"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, clocking_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    clocking = Repo.insert! %Clocking{}
    conn = get conn, clocking_path(conn, :edit, clocking)
    assert html_response(conn, 200) =~ "Edit clocking"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    clocking = Repo.insert! %Clocking{}
    conn = put conn, clocking_path(conn, :update, clocking), clocking: @valid_attrs
    assert redirected_to(conn) == clocking_path(conn, :show, clocking)
    assert Repo.get_by(Clocking, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    clocking = Repo.insert! %Clocking{}
    conn = put conn, clocking_path(conn, :update, clocking), clocking: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit clocking"
  end

  test "deletes chosen resource", %{conn: conn} do
    clocking = Repo.insert! %Clocking{}
    conn = delete conn, clocking_path(conn, :delete, clocking)
    assert redirected_to(conn) == clocking_path(conn, :index)
    refute Repo.get(Clocking, clocking.id)
  end
end
