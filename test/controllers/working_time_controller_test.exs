defmodule Api.WorkingTimeControllerTest do
  use Api.ConnCase

  alias Api.WorkingTime
  @valid_attrs %{end: ~N[2010-04-17 14:00:00.000000], start: ~N[2010-04-17 14:00:00.000000]}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, working_time_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing working time"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, working_time_path(conn, :new)
    assert html_response(conn, 200) =~ "New working time"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, working_time_path(conn, :create), working_time: @valid_attrs
    working_time = Repo.get_by!(WorkingTime, @valid_attrs)
    assert redirected_to(conn) == working_time_path(conn, :show, working_time.id)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, working_time_path(conn, :create), working_time: @invalid_attrs
    assert html_response(conn, 200) =~ "New working time"
  end

  test "shows chosen resource", %{conn: conn} do
    working_time = Repo.insert! %WorkingTime{}
    conn = get conn, working_time_path(conn, :show, working_time)
    assert html_response(conn, 200) =~ "Show working time"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, working_time_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    working_time = Repo.insert! %WorkingTime{}
    conn = get conn, working_time_path(conn, :edit, working_time)
    assert html_response(conn, 200) =~ "Edit working time"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    working_time = Repo.insert! %WorkingTime{}
    conn = put conn, working_time_path(conn, :update, working_time), working_time: @valid_attrs
    assert redirected_to(conn) == working_time_path(conn, :show, working_time)
    assert Repo.get_by(WorkingTime, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    working_time = Repo.insert! %WorkingTime{}
    conn = put conn, working_time_path(conn, :update, working_time), working_time: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit working time"
  end

  test "deletes chosen resource", %{conn: conn} do
    working_time = Repo.insert! %WorkingTime{}
    conn = delete conn, working_time_path(conn, :delete, working_time)
    assert redirected_to(conn) == working_time_path(conn, :index)
    refute Repo.get(WorkingTime, working_time.id)
  end
end
