defmodule Api.APITest do
  use Api.DataCase

  alias Api.API

  describe "user" do
    alias Api.API.User

    @valid_attrs %{email: "some email", username: "some username"}
    @update_attrs %{email: "some updated email", username: "some updated username"}
    @invalid_attrs %{email: nil, username: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_user()

      user
    end

    test "list_user/0 returns all user" do
      user = user_fixture()
      assert API.list_user() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert API.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = API.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = API.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.email == "some updated email"
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_user(user, @invalid_attrs)
      assert user == API.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = API.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> API.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = API.change_user(user)
    end
  end

  describe "clocking" do
    alias Api.API.Clocking

    @valid_attrs %{status: true, time: ~N[2010-04-17 14:00:00.000000]}
    @update_attrs %{status: false, time: ~N[2011-05-18 15:01:01.000000]}
    @invalid_attrs %{status: nil, time: nil}

    def clocking_fixture(attrs \\ %{}) do
      {:ok, clocking} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_clocking()

      clocking
    end

    test "list_clocking/0 returns all clocking" do
      clocking = clocking_fixture()
      assert API.list_clocking() == [clocking]
    end

    test "get_clocking!/1 returns the clocking with given id" do
      clocking = clocking_fixture()
      assert API.get_clocking!(clocking.id) == clocking
    end

    test "create_clocking/1 with valid data creates a clocking" do
      assert {:ok, %Clocking{} = clocking} = API.create_clocking(@valid_attrs)
      assert clocking.status == true
      assert clocking.time == ~N[2010-04-17 14:00:00.000000]
    end

    test "create_clocking/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_clocking(@invalid_attrs)
    end

    test "update_clocking/2 with valid data updates the clocking" do
      clocking = clocking_fixture()
      assert {:ok, clocking} = API.update_clocking(clocking, @update_attrs)
      assert %Clocking{} = clocking
      assert clocking.status == false
      assert clocking.time == ~N[2011-05-18 15:01:01.000000]
    end

    test "update_clocking/2 with invalid data returns error changeset" do
      clocking = clocking_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_clocking(clocking, @invalid_attrs)
      assert clocking == API.get_clocking!(clocking.id)
    end

    test "delete_clocking/1 deletes the clocking" do
      clocking = clocking_fixture()
      assert {:ok, %Clocking{}} = API.delete_clocking(clocking)
      assert_raise Ecto.NoResultsError, fn -> API.get_clocking!(clocking.id) end
    end

    test "change_clocking/1 returns a clocking changeset" do
      clocking = clocking_fixture()
      assert %Ecto.Changeset{} = API.change_clocking(clocking)
    end
  end

  describe "working_time" do
    alias Api.API.WorkingTime

    @valid_attrs %{end: ~N[2010-04-17 14:00:00.000000], start: ~N[2010-04-17 14:00:00.000000]}
    @update_attrs %{end: ~N[2011-05-18 15:01:01.000000], start: ~N[2011-05-18 15:01:01.000000]}
    @invalid_attrs %{end: nil, start: nil}

    def working_time_fixture(attrs \\ %{}) do
      {:ok, working_time} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_working_time()

      working_time
    end

    test "list_working_time/0 returns all working_time" do
      working_time = working_time_fixture()
      assert API.list_working_time() == [working_time]
    end

    test "get_working_time!/1 returns the working_time with given id" do
      working_time = working_time_fixture()
      assert API.get_working_time!(working_time.id) == working_time
    end

    test "create_working_time/1 with valid data creates a working_time" do
      assert {:ok, %WorkingTime{} = working_time} = API.create_working_time(@valid_attrs)
      assert working_time.end == ~N[2010-04-17 14:00:00.000000]
      assert working_time.start == ~N[2010-04-17 14:00:00.000000]
    end

    test "create_working_time/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_working_time(@invalid_attrs)
    end

    test "update_working_time/2 with valid data updates the working_time" do
      working_time = working_time_fixture()
      assert {:ok, working_time} = API.update_working_time(working_time, @update_attrs)
      assert %WorkingTime{} = working_time
      assert working_time.end == ~N[2011-05-18 15:01:01.000000]
      assert working_time.start == ~N[2011-05-18 15:01:01.000000]
    end

    test "update_working_time/2 with invalid data returns error changeset" do
      working_time = working_time_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_working_time(working_time, @invalid_attrs)
      assert working_time == API.get_working_time!(working_time.id)
    end

    test "delete_working_time/1 deletes the working_time" do
      working_time = working_time_fixture()
      assert {:ok, %WorkingTime{}} = API.delete_working_time(working_time)
      assert_raise Ecto.NoResultsError, fn -> API.get_working_time!(working_time.id) end
    end

    test "change_working_time/1 returns a working_time changeset" do
      working_time = working_time_fixture()
      assert %Ecto.Changeset{} = API.change_working_time(working_time)
    end
  end

  describe "teams" do
    alias Api.API.Teams

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def teams_fixture(attrs \\ %{}) do
      {:ok, teams} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_teams()

      teams
    end

    test "list_teams/0 returns all teams" do
      teams = teams_fixture()
      assert API.list_teams() == [teams]
    end

    test "get_teams!/1 returns the teams with given id" do
      teams = teams_fixture()
      assert API.get_teams!(teams.id) == teams
    end

    test "create_teams/1 with valid data creates a teams" do
      assert {:ok, %Teams{} = teams} = API.create_teams(@valid_attrs)
    end

    test "create_teams/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_teams(@invalid_attrs)
    end

    test "update_teams/2 with valid data updates the teams" do
      teams = teams_fixture()
      assert {:ok, teams} = API.update_teams(teams, @update_attrs)
      assert %Teams{} = teams
    end

    test "update_teams/2 with invalid data returns error changeset" do
      teams = teams_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_teams(teams, @invalid_attrs)
      assert teams == API.get_teams!(teams.id)
    end

    test "delete_teams/1 deletes the teams" do
      teams = teams_fixture()
      assert {:ok, %Teams{}} = API.delete_teams(teams)
      assert_raise Ecto.NoResultsError, fn -> API.get_teams!(teams.id) end
    end

    test "change_teams/1 returns a teams changeset" do
      teams = teams_fixture()
      assert %Ecto.Changeset{} = API.change_teams(teams)
    end
  end

  describe "users_teams" do
    alias Api.API.UserTeams

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def user_teams_fixture(attrs \\ %{}) do
      {:ok, user_teams} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_user_teams()

      user_teams
    end

    test "list_users_teams/0 returns all users_teams" do
      user_teams = user_teams_fixture()
      assert API.list_users_teams() == [user_teams]
    end

    test "get_user_teams!/1 returns the user_teams with given id" do
      user_teams = user_teams_fixture()
      assert API.get_user_teams!(user_teams.id) == user_teams
    end

    test "create_user_teams/1 with valid data creates a user_teams" do
      assert {:ok, %UserTeams{} = user_teams} = API.create_user_teams(@valid_attrs)
    end

    test "create_user_teams/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_user_teams(@invalid_attrs)
    end

    test "update_user_teams/2 with valid data updates the user_teams" do
      user_teams = user_teams_fixture()
      assert {:ok, user_teams} = API.update_user_teams(user_teams, @update_attrs)
      assert %UserTeams{} = user_teams
    end

    test "update_user_teams/2 with invalid data returns error changeset" do
      user_teams = user_teams_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_user_teams(user_teams, @invalid_attrs)
      assert user_teams == API.get_user_teams!(user_teams.id)
    end

    test "delete_user_teams/1 deletes the user_teams" do
      user_teams = user_teams_fixture()
      assert {:ok, %UserTeams{}} = API.delete_user_teams(user_teams)
      assert_raise Ecto.NoResultsError, fn -> API.get_user_teams!(user_teams.id) end
    end

    test "change_user_teams/1 returns a user_teams changeset" do
      user_teams = user_teams_fixture()
      assert %Ecto.Changeset{} = API.change_user_teams(user_teams)
    end
  end

  describe "batsignal" do
    alias Api.API.Batsignal

    @valid_attrs %{signal: true}
    @update_attrs %{signal: false}
    @invalid_attrs %{signal: nil}

    def batsignal_fixture(attrs \\ %{}) do
      {:ok, batsignal} =
        attrs
        |> Enum.into(@valid_attrs)
        |> API.create_batsignal()

      batsignal
    end

    test "list_batsignal/0 returns all batsignal" do
      batsignal = batsignal_fixture()
      assert API.list_batsignal() == [batsignal]
    end

    test "get_batsignal!/1 returns the batsignal with given id" do
      batsignal = batsignal_fixture()
      assert API.get_batsignal!(batsignal.id) == batsignal
    end

    test "create_batsignal/1 with valid data creates a batsignal" do
      assert {:ok, %Batsignal{} = batsignal} = API.create_batsignal(@valid_attrs)
      assert batsignal.signal == true
    end

    test "create_batsignal/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = API.create_batsignal(@invalid_attrs)
    end

    test "update_batsignal/2 with valid data updates the batsignal" do
      batsignal = batsignal_fixture()
      assert {:ok, batsignal} = API.update_batsignal(batsignal, @update_attrs)
      assert %Batsignal{} = batsignal
      assert batsignal.signal == false
    end

    test "update_batsignal/2 with invalid data returns error changeset" do
      batsignal = batsignal_fixture()
      assert {:error, %Ecto.Changeset{}} = API.update_batsignal(batsignal, @invalid_attrs)
      assert batsignal == API.get_batsignal!(batsignal.id)
    end

    test "delete_batsignal/1 deletes the batsignal" do
      batsignal = batsignal_fixture()
      assert {:ok, %Batsignal{}} = API.delete_batsignal(batsignal)
      assert_raise Ecto.NoResultsError, fn -> API.get_batsignal!(batsignal.id) end
    end

    test "change_batsignal/1 returns a batsignal changeset" do
      batsignal = batsignal_fixture()
      assert %Ecto.Changeset{} = API.change_batsignal(batsignal)
    end
  end
end
