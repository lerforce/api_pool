defmodule ApiWeb.UserTeamsControllerTest do
  use ApiWeb.ConnCase

  alias Api.API
  alias Api.API.UserTeams

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:user_teams) do
    {:ok, user_teams} = API.create_user_teams(@create_attrs)
    user_teams
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users_teams", %{conn: conn} do
      conn = get conn, user_teams_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create user_teams" do
    test "renders user_teams when data is valid", %{conn: conn} do
      conn = post conn, user_teams_path(conn, :create), user_teams: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, user_teams_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, user_teams_path(conn, :create), user_teams: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user_teams" do
    setup [:create_user_teams]

    test "renders user_teams when data is valid", %{conn: conn, user_teams: %UserTeams{id: id} = user_teams} do
      conn = put conn, user_teams_path(conn, :update, user_teams), user_teams: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, user_teams_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn, user_teams: user_teams} do
      conn = put conn, user_teams_path(conn, :update, user_teams), user_teams: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user_teams" do
    setup [:create_user_teams]

    test "deletes chosen user_teams", %{conn: conn, user_teams: user_teams} do
      conn = delete conn, user_teams_path(conn, :delete, user_teams)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, user_teams_path(conn, :show, user_teams)
      end
    end
  end

  defp create_user_teams(_) do
    user_teams = fixture(:user_teams)
    {:ok, user_teams: user_teams}
  end
end
