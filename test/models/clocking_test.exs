defmodule Api.ClockingTest do
  use Api.ModelCase

  alias Api.Clocking

  @valid_attrs %{status: true, time: ~N[2010-04-17 14:00:00.000000]}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Clocking.changeset(%Clocking{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Clocking.changeset(%Clocking{}, @invalid_attrs)
    refute changeset.valid?
  end
end
