defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  pipeline :csrf do
    plug :protect_from_forgery
  end

  pipeline :auth do
    plug AuthPlug
  end

  scope "/", ApiWeb do
    pipe_through [:api] # Use the default browser stack

    options "/login", AuthController, :options
    post "/login", AuthController, :create

    options "/api/users/:id", UserController, :options
    options "/api/users", UserController, :options

    options "/api/clocks/:id", ClockingController, :options
    options "/api/clocks", ClockingController, :options
    options "/api/clocks/last/:user_id", ClockingController, :options

    options "/api/workingtimes/:id", WorkingTimeController, :options
    options "/api/workingtimes", WorkingTimeController, :options

    options "/api/teams/:id", TeamsController, :options
    options "/api/teams", TeamsController, :options
    options "/api/teams/remove/:id", TeamsController, :options

    options "/api/batsignal/:id", BatsignalController, :options
  end

  scope "/api", ApiWeb do
    pipe_through [:auth, :api]

    resources "/users", UserController

    post "/clocks/:user", ClockingController, :create
    get "/clocks/:user_id", ClockingController, :index
    get "/clocks/last/:user_id", ClockingController, :last_clocking

    post "/workingtimes/:user", WorkingTimeController, :create
    get "/workingtimes/:user_id", WorkingTimeController, :index
    get "/workingtimes/:user_id/:working_time_id", WorkingTimeController, :show
    put "/workingtimes/:id", WorkingTimeController, :update
    delete "/workingtimes/:id", WorkingTimeController, :delete

    post "/teams", TeamsController, :create
    get "/teams/:user_id", TeamsController, :show
    put "/teams/:teams_id", TeamsController, :update
    put "/teams/remove/:id", TeamsController, :remove_employe

    get "/batsignal/:id", BatsignalController, :show
    put "/batsignal/:id", BatsignalController, :update
  end

  # Other scopes may use custom stacks.
  # scope "/api", ApiWeb do
  #   pipe_through :api
  # end
end
