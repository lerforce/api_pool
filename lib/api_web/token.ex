defmodule ApiWeb.Token do
  use Joken.Config

  @signer Joken.Signer.create("HS256", "my_secret")

  def build_token(user) do
    extra_claims = %{user_id: user.id, user_email: user.email}
    Joken.generate_and_sign!(%{}, extra_claims, @signer)
  end

  def verify_token(token) do
    Joken.verify(token, @signer)
  end

end