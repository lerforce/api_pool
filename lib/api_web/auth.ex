defmodule AuthPlug do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, params) do
    token = conn
            |> get_req_header("authorization")
    if (token == []) do
      handle_error(conn)
    end
    token = List.first(token)
    if (!ApiWeb.Token.verify_token(token)) do
      handle_error(conn)
    else
      conn
    end
  end

  defp handle_error(conn) do
    conn
    |> send_resp(401, "Invalid Token")
  end

end