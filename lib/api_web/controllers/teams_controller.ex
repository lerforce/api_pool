defmodule ApiWeb.TeamsController do
  use ApiWeb, :controller

  alias Api.API
  alias Api.API.Teams

  action_fallback ApiWeb.FallbackController

  def options(conn, %{"id" => _}) do end

  def options(conn, _) do end

  def create(conn, %{"teams" => teams_params}) do
    manager = API.get_user!(teams_params["manager_id"])
    with {:ok, %Teams{} = teams} <- API.create_teams(teams_params, manager) do
      conn
      |> put_status(:created)
      |> render("show.json", %{teams: teams, manager: manager, employes: %{}})
    end
  end

  def show(conn, %{"user_id" => user_id}) do
    teams = API.get_teams!(user_id)
    if (teams == nil) do
      render(conn, "show.json", %{teams: nil, manager: nil, employes: nil})
    end
    user_teams = API.get_all_user_teams_by_teams_id(teams.id)
    employes = API.get_all_users_by_id(user_teams)
    render(conn, "show.json", %{teams: teams, manager: teams.user, employes: employes})
  end

  def update(conn, %{"teams_id" => id, "teams" => teams_params}) do
    teams = API.get_teams_by_id!(id)
    users = API.get_all_users_by_id(teams_params["employes"])
    for user <- users do
      API.create_user_teams(user, teams)
    end
    user_teams = API.get_all_user_teams_by_teams_id(teams.id)
    employes = API.get_all_users_by_id(user_teams)
    with {:ok, %Teams{} = teams} <- API.update_teams(teams, users, teams_params) do
      render(conn, "show.json", %{teams: teams, manager: teams.user, employes: employes})
    end
  end

  def remove_employe(conn, %{"id" => id, "teams" => teams_params}) do
    teams = API.get_teams_by_id!(id)
    count = length(teams_params["employes"])
    manager = teams.user
    with {count, nil} <- API.delete_user_teams(teams.id, teams_params["employes"]) do
      user_teams = API.get_all_user_teams_by_teams_id(teams.id)
      employes = API.get_all_users_by_id(user_teams)
      render(conn, "show.json", %{teams: teams, manager: manager, employes: employes})
    end
  end

end
