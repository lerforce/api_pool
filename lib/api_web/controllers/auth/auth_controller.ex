defmodule ApiWeb.AuthController do
  use ApiWeb, :controller

  alias Api.API
  alias ApiWeb.Token

  def options(conn, _) do  end

  def create(conn, params) do
    if !Map.has_key?(params, "email") || !Map.has_key?(params, "password") do
      error_gesture(conn)
    end
    user = API.get_users_by_email!(params["email"])
    if (user == nil) do error_gesture(conn) end
    returned_user = API.authenticate(user, params["password"])
    if returned_user == nil do
      conn
      |> put_status(401)
      |> render("error.json", message: "Invalid credentials")
    else
      jwt = Token.build_token(user)
      conn
      |> put_resp_header("Authorization", jwt)
      |> put_status(:ok)
      |> render("auth.json", %{user: user, token: jwt})
    end
  end

  defp error_gesture(conn) do
    conn
    |> put_status(401)
    |> render("error.json", message: "invalid request")
  end

end