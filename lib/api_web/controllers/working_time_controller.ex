defmodule ApiWeb.WorkingTimeController do
  use ApiWeb, :controller

  alias Api.API
  alias Api.API.WorkingTime

  action_fallback ApiWeb.FallbackController

  def options(_conn, %{"id" => _}) do  end

  def options(_conn, %{}) do  end

  def index(conn, %{"user_id" => user_id, "start" => start}) do
    working_times = API.get_working_time_by_user_id_and_start(user_id, start)
    render(conn, "index.json", working_time: working_times)
  end

  def index(conn, %{"user_id" => user_id, "end" => end_datetime}) do
    working_times = API.get_working_time_by_user_id_and_end(user_id, end_datetime)
    render(conn, "index.json", working_time: working_times)
  end

  def index(conn, %{"user_id" => user_id, "start" => start_time, "end" => end_datetime}) do
    working_times = API.get_working_time_by_user_id_start_and_end(user_id, start_time, end_datetime)
    render(conn, "index.json", working_time: working_times)
  end


  def index(conn, %{"user_id" => user_id}) do
    working_times = API.get_working_time_by_user_id(user_id)
    render(conn, "index.json", working_time: working_times)
  end

  def create(conn, %{"user" => userid, "working_time" => working_time_params}) do
    user = API.get_user!(userid)
    with {:ok, %WorkingTime{} = working_time} <- API.create_working_time(user, working_time_params) do
      conn
      |> put_status(:created)
      |> render("show.json", working_time: working_time)
    end
  end

  def show(conn, %{"user_id" => user_id, "working_time_id" => working_time_id}) do
    working_time = API.get_working_time_by_id_and_user_id(working_time_id, user_id)
    render(conn, "show.json", working_time: working_time)
  end

  def update(conn, %{"id" => id, "working_time" => working_time_params}) do
    working_time = API.get_working_time_by_id(id)

    with {:ok, %WorkingTime{} = working_time} <- API.update_working_time(working_time, working_time_params) do
      render(conn, "show.json", working_time: working_time)
    end
  end

  def delete(conn, %{"id" => id}) do
    working_time = API.get_working_time_by_id(id)
    with {:ok, %WorkingTime{}} <- API.delete_working_time(working_time) do
      send_resp(conn, :ok, "")
    end
  end
end
