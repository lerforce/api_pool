defmodule ApiWeb.BatsignalController do
  use ApiWeb, :controller

  alias Api.API
  alias Api.API.Batsignal

  action_fallback ApiWeb.FallbackController

  def options(conn, %{"id" => _}) do end

  def show(conn, %{"id" => id}) do
    batsignal = API.get_batsignal!(id)
    render(conn, "show.json", batsignal: batsignal)
  end

  def update(conn, %{"id" => id, "batsignal" => batsignal_params}) do
    batsignal = API.get_batsignal!(id)

    with {:ok, %Batsignal{} = batsignal} <- API.update_batsignal(batsignal, batsignal_params) do
      render(conn, "show.json", batsignal: batsignal)
    end
  end

end
