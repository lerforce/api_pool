defmodule ApiWeb.ClockingController do
  use ApiWeb, :controller

  alias Api.API
  alias Api.API.Clocking

  action_fallback ApiWeb.FallbackController

  def options(_conn, %{}) do  end

  def options(_conn, %{"id" => _}) do end

  def create(conn, %{"user" => user_id, "clocking" => clocking_params}) do
    user = API.get_user!(user_id)
    with {:ok, %Clocking{} = clocking} <- API.create_clocking(user, clocking_params) do
      conn
      |> put_status(:created)
      |> render("show.json", clocking: clocking)
    end
  end

  def index(conn, %{"user_id" => user_id}) do
    case API.get_user!(user_id) do
      nil -> {:error, :not_found}
      user -> clockings = API.get_clockings_by_user_id(user.id)
              render(conn, "index.json", clocking: clockings)
    end
  end

  def last_clocking(conn, %{"user_id" => user_id}) do
    case API.get_last_clocking(user_id) do
      nil ->  {:error, :not_found}
      clocking -> render(conn, "show.json", clocking: clocking)
    end
  end
end
