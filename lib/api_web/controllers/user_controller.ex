defmodule ApiWeb.UserController do
  use ApiWeb, :controller

  alias Api.API
  alias Api.API.User

  action_fallback ApiWeb.FallbackController

  def options(_conn, %{}) do  end

  def options(_conn, %{"id" => _}) do end

  def index(conn, %{"username" => username, "email" => email}) do
    user = API.get_users_by_username_and_email!(username, email)
    render(conn, "index.json", user: user)
  end

  def index(conn, %{"email" => email}) do
    user = API.get_users_by_email!(email)
    IO.inspect(user)
    render(conn, "show.json", user: user)
  end

  def index(conn, %{"username" => username}) do
    user = API.get_users_by_username!(username)
    render(conn, "index.json", user: user)
  end

  def index(conn, _params) do
    user = API.list_user()
    render(conn, "index.json", user: user)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- API.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = API.get_user!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = API.get_user!(id)

    with {:ok, %User{} = user} <- API.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = API.get_user!(id)
    with {:ok, %User{}} <- API.delete_user(user) do
      send_resp(conn, :ok, "")
    end
  end
end
