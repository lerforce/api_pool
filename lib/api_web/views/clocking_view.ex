defmodule ApiWeb.ClockingView do
  use ApiWeb, :view
  alias ApiWeb.ClockingView

  def render("index.json", %{clocking: clocking}) do
    %{data: render_many(clocking, ClockingView, "clocking.json")}
  end

  def render("show.json", %{clocking: clocking}) do
    %{data: render_one(clocking, ClockingView, "clocking.json")}
  end

  def render("clocking.json", %{clocking: clocking}) do
    %{id: clocking.id,
      time: clocking.time,
      status: clocking.status}
  end
end
