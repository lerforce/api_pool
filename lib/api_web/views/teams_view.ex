defmodule ApiWeb.TeamsView do
  use ApiWeb, :view
  alias ApiWeb.TeamsView

  def render("index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamsView, "teams.json")}
  end

  def render("show.json", %{teams: teams, manager: manager, employes: employes}) do
    %{data: render_one(teams, TeamsView, "teams.json", %{teams: teams, manager: manager, employes: employes})}
  end

  def render("teams.json", %{teams: teams, manager: manager, employes: employes}) do
    %{
      id: teams.id,
      name: teams.name,
      manager: render_one(manager, ApiWeb.UserView, "user.json"),
      employes: render_many(employes, ApiWeb.UserView, "user_teams.json")
    }
  end
end
