defmodule ApiWeb.WorkingTimeView do
  use ApiWeb, :view
  alias ApiWeb.WorkingTimeView

  def render("index.json", %{working_time: working_time}) do
    %{data: render_many(working_time, WorkingTimeView, "working_time.json")}
  end

  def render("show.json", %{working_time: working_time}) do
    %{data: render_one(working_time, WorkingTimeView, "working_time.json")}
  end

  def render("working_time.json", %{working_time: working_time}) do
    start = working_time.start
    start_date_time = "#{start.year}-#{start.month}-#{start.day} #{start.hour}:#{start.minute}:#{start.second}"
    end_date = working_time.end
    end_date_time = "#{end_date.year}-#{end_date.month}-#{end_date.day} #{end_date.hour}:#{end_date.minute}:#{end_date.second}"
    %{id: working_time.id,
      start: start_date_time,
      end: end_date_time}
  end
end
