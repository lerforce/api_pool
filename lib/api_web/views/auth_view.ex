defmodule ApiWeb.AuthView do
  use ApiWeb, :view
  alias ApiWeb.AuthView

  def render("error.json", %{message: message}) do
    %{
      message: message
    }
  end

  def render("show.json", %{user: user, token: token}) do
    %{data: render_one(%{user: user, token: token}, ApiWeb.AuthView, "auth.json")}
  end

  def render("auth.json", %{user: user, token: token}) do
    %{
      user: %{
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role
      },
      token: token
    }
  end


end