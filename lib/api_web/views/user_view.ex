defmodule ApiWeb.UserView do
  use ApiWeb, :view
  alias ApiWeb.UserView

  def render("index.json", %{user: user}) do
    %{data: render_many(user, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      role: user.role
    }
  end

  def render("user_teams.json", %{user: user}) do
    %{
      user: %{
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
      },
      working_times: render_many(user.working_time, ApiWeb.WorkingTimeView, "working_time.json"),
      clockings: render_many(user.clocking, ApiWeb.ClockingView, "clocking.json")
    }
  end

end
