defmodule ApiWeb.BatsignalView do
  use ApiWeb, :view
  alias ApiWeb.BatsignalView

  def render("index.json", %{batsignal: batsignal}) do
    %{data: render_many(batsignal, BatsignalView, "batsignal.json")}
  end

  def render("show.json", %{batsignal: batsignal}) do
    %{data: render_one(batsignal, BatsignalView, "batsignal.json")}
  end

  def render("batsignal.json", %{batsignal: batsignal}) do
    %{id: batsignal.id,
      signal: batsignal.signal}
  end
end
