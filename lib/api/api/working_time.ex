defmodule Api.API.WorkingTime do
  use Ecto.Schema
  import Ecto.Changeset


  schema "working_time" do
    field :end, :naive_datetime
    field :start, :naive_datetime
    belongs_to :user, Api.API.User, primary_key: true

    timestamps()
  end

  @doc false
  def changeset(working_time, attrs) do
    working_time
    |> cast(attrs, [:start, :end])
    |> validate_required([:start, :end])
    |> date_validation()
  end



defp date_validation(changeset) do
    starts_on = get_field(changeset, :start)
    ends_on = get_field(changeset, :end)

    if starts_on > ends_on do
      add_error(changeset, :starts_on, "cannot be later than 'end'")
    else
      changeset
    end
end
end
