defmodule Api.API.Clocking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "clocking" do
    field :status, :boolean, default: false
    field :time, :naive_datetime
    belongs_to :user, Api.API.User, primary_key: true

    timestamps()
  end

  @doc false

  def changeset(clocking, attrs) do
    clocking
    |> cast(attrs, [:time, :status])
    |> validate_required([:time, :status])
  end
end
