defmodule Api.API.User do
  use Ecto.Schema
  use Pow.Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :email, :string
    field :username, :string
    field :is_admin, :boolean
    field :password, :string
    field :role, :string, null: false, default: "user"
    has_many :working_time, Api.API.WorkingTime, on_delete: :delete_all
    has_many :clocking, Api.API.Clocking, on_delete: :delete_all
    has_many :teams, Api.API.Teams, on_delete: :delete_all
    has_many :user_teams, Api.API.UserTeams, on_delete: :delete_all
    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password])
    |> validate_required([:username, :email, :password])
    |> validate_format(
         :email,
         ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/,
         message: "must be an email"
       )
    |> validate_length(:password, min: 6)
    |> put_change(:password, Bcrypt.hash_pwd_salt(attrs["password"]))
    |> unique_constraint(:email)
  end

  @spec changeset_role(Ecto.Schema.t() | Ecto.Changeset.t(), map()) :: Ecto.Changeset.t()
  def changeset_role(user_or_changeset, attrs) do
    user_or_changeset
    |> Ecto.Changeset.cast(attrs, [:role])
    |> Ecto.Changeset.validate_inclusion(:role, ~w(user admin))
  end
end
