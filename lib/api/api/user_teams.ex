defmodule Api.API.UserTeams do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users_teams" do
    belongs_to :user, Api.API.User
    belongs_to :teams, Api.API.Teams

    timestamps()
  end

  @doc false
  def changeset(user_teams, attrs) do
    user_teams
    |> cast(attrs, [:user_id, :teams_id])
    |> validate_required([:user_id, :teams_id])
  end
end
