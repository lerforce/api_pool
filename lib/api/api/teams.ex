defmodule Api.API.Teams do
  use Ecto.Schema
  import Ecto.Changeset


  schema "teams" do
    field :name, :string
    belongs_to :user, Api.API.User, references: :id
    has_many :user_teams, Api.API.UserTeams, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(teams, attrs) do
    teams
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def changeset_put(teams, put_attrs) do
    teams
    |> cast(put_attrs, [])
    |> validate_required([])
  end
end
