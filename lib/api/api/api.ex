defmodule Api.API do
  @moduledoc """
  The API context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.API.User

  @doc """
  Returns the list of user.

  ## Examples

      iex> list_user()
      [%User{}, ...]

  """
  def list_user do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  def get_all_users_by_id(users_ids) do
    from(u in User, where: u.id in ^users_ids, preload: [:working_time, clocking: ^from(c in Api.API.Clocking, order_by: [asc: c.time])])
    |> Repo.all
  end


  def get_users_by_username!(username) do
    from(u in User, where: u.username == ^username)
    |> Repo.all()
  end

  def get_users_by_email!(email), do: Repo.get_by(User, email: email)

  def get_users_by_username_and_email!(username, email) do
    from(u in User, where: u.username == ^username and u.email == ^email)
    |> Repo.all()
  end


  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> User.changeset_role(attrs)
    |> Repo.insert()
  end

  def authenticate(user, password) do
    if Bcrypt.verify_pass(password, user.password) do
      user
    else
      nil
    end
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  alias Api.API.Clocking

  @doc """
  Returns the list of clocking.

  ## Examples

      iex> list_clocking()
      [%Clocking{}, ...]

  """
  def list_clocking do
    Repo.all(Clocking)
  end

  @doc """
  Gets a single clocking.

  Raises `Ecto.NoResultsError` if the Clocking does not exist.

  ## Examples

      iex> get_clocking!(123)
      %Clocking{}

      iex> get_clocking!(456)
      ** (Ecto.NoResultsError)

  """
  def get_clocking!(id), do: Repo.get!(Clocking, id)

  def get_last_clocking(user_id) do
    query = from c in Clocking, where: c.user_id ==^user_id, order_by: [desc: c.time], limit: 1
    list = Repo.all(query)
    List.first(list)
  end

  def get_clockings_by_user_id(user_id) do
    from(c in Clocking, where: c.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
  Creates a clocking.

  ## Examples

      iex> create_clocking(%{field: value})
      {:ok, %Clocking{}}

      iex> create_clocking(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_clocking(user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:clocking)
    |> Clocking.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a clocking.

  ## Examples

      iex> update_clocking(clocking, %{field: new_value})
      {:ok, %Clocking{}}

      iex> update_clocking(clocking, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_clocking(%Clocking{} = clocking, attrs) do
    clocking
    |> Clocking.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Clocking.

  ## Examples

      iex> delete_clocking(clocking)
      {:ok, %Clocking{}}

      iex> delete_clocking(clocking)
      {:error, %Ecto.Changeset{}}

  """
  def delete_clocking(%Clocking{} = clocking) do
    Repo.delete(clocking)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking clocking changes.

  ## Examples

      iex> change_clocking(clocking)
      %Ecto.Changeset{source: %Clocking{}}

  """
  def change_clocking(%Clocking{} = clocking) do
    Clocking.changeset(clocking, %{})
  end

  alias Api.API.WorkingTime

  @doc """
  Returns the list of working_time.

  ## Examples

      iex> list_working_time()
      [%WorkingTime{}, ...]

  """
  def list_working_time do
    Repo.all(WorkingTime)
  end

  @doc """
  Gets a single working_time.

  Raises `Ecto.NoResultsError` if the Working time does not exist.

  ## Examples

      iex> get_working_time!(123)
      %WorkingTime{}

      iex> get_working_time!(456)
      ** (Ecto.NoResultsError)

  """
  def get_working_time_by_id(id) do
    Repo.get_by!(WorkingTime, id: id)
  end


  def get_working_time_by_user_id(user_id) do
    from(w in WorkingTime, where: w.user_id == ^user_id)
    |> Repo.all()
  end

  def get_working_time_by_user_id_and_start(user_id, start_time) do
    from(w in WorkingTime, where: w.user_id == ^user_id and w.start == ^start_time)
    |> Repo.all()
  end

  def get_working_time_by_user_id_and_end(user_id, end_time) do
    from(w in WorkingTime, where: w.user_id == ^user_id and w.end == ^end_time)
    |> Repo.all()
  end

  def get_working_time_by_user_id_start_and_end(user_id, start_time, end_time) do
    from(w in WorkingTime, where: w.user_id == ^user_id and w.start == ^start_time and w.end == ^end_time)
    |> Repo.all()
  end

  def get_working_time_by_id_and_user_id(id, user_id) do
    from(w in WorkingTime, where: w.id == ^id and w.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
  Creates a working_time.

  ## Examples

      iex> create_working_time(%{field: value})
      {:ok, %WorkingTime{}}

      iex> create_working_time(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_working_time(user, attrs \\ %{}) do
    user
    |> Ecto.build_assoc(:working_time)
    |> WorkingTime.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a working_time.

  ## Examples

      iex> update_working_time(working_time, %{field: new_value})
      {:ok, %WorkingTime{}}

      iex> update_working_time(working_time, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_working_time(%WorkingTime{} = working_time, attrs) do
    working_time
    |> WorkingTime.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a WorkingTime.

  ## Examples

      iex> delete_working_time(working_time)
      {:ok, %WorkingTime{}}

      iex> delete_working_time(working_time)
      {:error, %Ecto.Changeset{}}

  """
  def delete_working_time(%WorkingTime{} = working_time) do
    Repo.delete(working_time)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking working_time changes.

  ## Examples

      iex> change_working_time(working_time)
      %Ecto.Changeset{source: %WorkingTime{}}

  """
  def change_working_time(%WorkingTime{} = working_time) do
    WorkingTime.changeset(working_time, %{})
  end

  alias Api.API.Teams

  @doc
  """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Teams{}, ...]

  """
  def list_teams do
    Repo.all(Teams)
  end

  @doc """
  Gets a single teams.

  Raises `Ecto.NoResultsError` if the Teams does not exist.

  ## Examples

      iex> get_teams!(123)
      %Teams{}

      iex> get_teams!(456)
      ** (Ecto.NoResultsError)

  """
  def get_teams!(id) do
    Repo.get_by(Teams, user_id: id)
    |> Repo.preload([:user])
  end

  def get_teams_by_id!(id), do: Repo.get!(Teams, id) |> Repo.preload([:user])

  def create_teams(attrs, manager) do
    manager
    |>Ecto.build_assoc(:teams)
    |> Teams.changeset(%{manager: manager, user_id: manager.id, name: attrs["name"]})
    |> Repo.insert()
  end

  def update_teams(teams, users, attrs) do
    teams
    |> Teams.changeset_put(%{})
    |> Repo.update()
  end

  def delete_teams(%Teams{} = teams) do
    Repo.delete(teams)
  end

  def change_teams(%Teams{} = teams) do
    Teams.changeset(teams, %{})
  end

  alias Api.API.UserTeams

  def list_users_teams do
    Repo.all(UserTeams)
  end

  def get_user_teams!(id), do: Repo.get!(UserTeams, id)

  def get_all_user_teams_by_teams_id(teams_id) do
    query = from(u in UserTeams, where: u.teams_id == ^teams_id, select: u.user_id)
    Repo.all(query)
  end

  def create_user_teams(user, teams) do
    user
    |> Ecto.build_assoc(:user_teams)
    |> UserTeams.changeset(%{user_id: user.id, teams_id: teams.id})
    |> Repo.insert()
  end

  def update_user_teams(%UserTeams{} = user_teams, attrs) do
    user_teams
    |> UserTeams.changeset(attrs)
    |> Repo.update()
  end

  def delete_user_teams(team_id, users_id) do
    query = from(u in UserTeams, where: u.user_id in ^users_id and u.teams_id ==^team_id)
    Repo.delete_all(query)
  end

  def change_user_teams(%UserTeams{} = user_teams) do
    UserTeams.changeset(user_teams, %{})
  end

  alias Api.API.Batsignal

  @doc """
  Returns the list of batsignal.

  ## Examples

      iex> list_batsignal()
      [%Batsignal{}, ...]

  """
  def list_batsignal do
    Repo.all(Batsignal)
  end

  @doc """
  Gets a single batsignal.

  Raises `Ecto.NoResultsError` if the Batsignal does not exist.

  ## Examples

      iex> get_batsignal!(123)
      %Batsignal{}

      iex> get_batsignal!(456)
      ** (Ecto.NoResultsError)

  """
  def get_batsignal!(id), do: Repo.get!(Batsignal, id)

  @doc """
  Creates a batsignal.

  ## Examples

      iex> create_batsignal(%{field: value})
      {:ok, %Batsignal{}}

      iex> create_batsignal(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_batsignal(attrs \\ %{}) do
    %Batsignal{}
    |> Batsignal.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a batsignal.

  ## Examples

      iex> update_batsignal(batsignal, %{field: new_value})
      {:ok, %Batsignal{}}

      iex> update_batsignal(batsignal, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_batsignal(%Batsignal{} = batsignal, attrs) do
    batsignal
    |> Batsignal.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Batsignal.

  ## Examples

      iex> delete_batsignal(batsignal)
      {:ok, %Batsignal{}}

      iex> delete_batsignal(batsignal)
      {:error, %Ecto.Changeset{}}

  """
  def delete_batsignal(%Batsignal{} = batsignal) do
    Repo.delete(batsignal)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking batsignal changes.

  ## Examples

      iex> change_batsignal(batsignal)
      %Ecto.Changeset{source: %Batsignal{}}

  """
  def change_batsignal(%Batsignal{} = batsignal) do
    Batsignal.changeset(batsignal, %{})
  end
end
