defmodule Api.API.Batsignal do
  use Ecto.Schema
  import Ecto.Changeset


  schema "batsignal" do
    field :signal, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(batsignal, attrs) do
    batsignal
    |> cast(attrs, [:signal])
    |> validate_required([:signal])
  end
end
